import React from "react";
import { connect } from 'react-redux';
import {
  Alert,
  Button,
  Col,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form, FormGroup, FormFeedback, Label, Input
} from "reactstrap";
import { UserAction } from '../../../../actions'

class CreateUser extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      formData: {},
    };

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidUpdate() {
    const { isOpen, storeUser } = this.props;
    const { formData } = this.state;

    // open modal and check if storeuser has data
    if (!isOpen && Object.entries(storeUser).length ) {
      this.props.cleanStoreUser();
    }

    if (!isOpen && Object.entries(formData).length) {
      this.props.cleanStoreError();
      this.setState({ formData: {} });
    }

    if (Object.entries(storeUser).length && Object.entries(formData).length) {
      this.setState({ formData: {} });
    }
  }

  onChange(e) {
    const { storeUser } = this.props;
    const { name, value } = e.target;

    if (Object.entries(storeUser).length) {
      this.props.cleanStoreUser()
    }

    this.setState({
      formData: { ...this.state.formData, [name]: value }
    })   
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.postUser(this.state.formData);
  }

  render() {
    const {
      isUserFetching,
      storeError,
      toggleFn,
      storeUser,
      isOpen,
    } = this.props;
    const { formData } = this.state;

    return(
      <Modal isOpen={isOpen} size="lg" toggle={toggleFn} className={this.props.className}>
        <ModalHeader toggle={toggleFn}>Create User</ModalHeader>
        <ModalBody>
          <Form onSubmit={ this.onSubmit } ref="form">
          <Alert color="success" isOpen={ Object.entries(storeUser).length ? true : false }>
              You have successfully created new user.
            </Alert>
            <br />
              <h6>Personal Information</h6>
            <hr />
            <FormGroup row>
              <Label for="exampleEmail" sm={2}>Name</Label>
              <Col sm={5}>
                <Input type="text" name="first_name" placeholder="First Name..." value={ 'first_name' in formData ? formData.first_name: '' } onChange={ this.onChange } invalid={ 'first_name' in storeError } />
                <FormFeedback>{ 'first_name' in storeError && storeError['first_name'][0] }</FormFeedback>
              </Col>
              <Col sm={5}>
                <Input type="text" name="last_name" placeholder="Last Name..." value={ 'last_name' in formData ? formData.last_name: '' } onChange={ this.onChange } invalid={ 'last_name' in storeError } />
                <FormFeedback>{ 'last_name' in storeError && storeError['last_name'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="exampleText" sm={2}>Address</Label>
              <Col sm={10}>
                <Input type="text" rows="3" name="address" placeholder="Address..." value={ 'address' in formData ? formData.address: '' } onChange={ this.onChange }  invalid={ 'address' in storeError }/>
                <FormFeedback>{ 'address' in storeError && storeError['address'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="exampleEmail" sm={2}>Contact</Label>
              <Col sm={5}>
                <Input type="text" name="phone_no" placeholder="Phone No..." value={ 'phone_no' in formData ? formData.phone_no: '' } onChange={ this.onChange } invalid={ 'phone_no' in storeError }/>
                <FormFeedback>{ 'phone_no' in storeError && storeError['phone_no'][0] }</FormFeedback>
              </Col>
              <Col sm={5}>
                <Input type="text" name="telephone_no" placeholder="Telephone No..." value={ 'telephone_no' in formData ? formData.telephone_no: '' } onChange={ this.onChange } invalid={ 'telephone_no' in storeError } />
                <FormFeedback>{ 'telephone_no' in storeError && storeError['telephone_no'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            
            <br />
            <div className="clearfix"></div> 
            <h6>Group</h6>
            <hr />

            <FormGroup row>
              <Label for="exampleSelect" sm={2}>Department</Label>
              <Col sm={10}>
                <Input type="select" name="department" value={ 'department' in formData ? formData.department: '' } onChange={ this.onChange } invalid={ 'department' in storeError }>
                  { this.props.departments.map((e, key) => {
                    return <option key={key} value={ e._id }>{ e.name }</option>
                  }) }
                </Input>
                <FormFeedback>{ 'department' in storeError && storeError['department'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="exampleSelect" sm={2}>Bot</Label>
              <Col sm={10}>
                <Input type="select" name="bot" value={ 'bot' in formData ? formData.bot: '' } onChange={ this.onChange } invalid={ 'bot' in storeError }>
                  { this.props.bots.map((e, key) => {
                    return <option key={key} value={ e._id }>{ e.name }</option>
                  }) }
                </Input>
                <FormFeedback>{ 'bot' in storeError && storeError['bot'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <br />
            <h6>credentials</h6>
            <hr />

            <FormGroup row>
              <Label for="exampleSelect" sm={2}>Email</Label>
              <Col sm={10}>
                <Input type="text" name="email" placeholder="Email..." value={ 'email' in formData ? formData.email: '' } onChange={ this.onChange } invalid={ 'email' in storeError }/>
                <FormFeedback>{ 'email' in storeError && storeError['email'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="exampleSelect" sm={2}>Password</Label>
              <Col sm={10}>
                <Input type="password" name="password" placeholder="*******" value={ 'password' in formData ? formData.password: '' } onChange={ this.onChange } invalid={ 'password' in storeError } />
                <FormFeedback>{ 'password' in storeError && storeError['password'][0] }</FormFeedback>
              </Col>
            </FormGroup>
            <Button className="hidden" type="submit"></Button>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button 
            color="primary"
            onClick={this.onSubmit}
            disabled={ isUserFetching }
          >Submit</Button>{' '}
          <Button color="secondary" onClick={toggleFn}>Cancel</Button>
        </ModalFooter>
      </Modal>
    ) 
  }
}

const mapStateToProps = ({ user, bot, department }) => {
  return {
    users: user.data,
    storeError: user.storeError,
    bots: bot.bots,
    departments: department.departments,
    isUserFetching: user.isStoreFetching,
    storeUser: user.storeUser,
    formData: user.storePendingUser
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    postUser: (data) => dispatch( UserAction.post(data) ),
    cleanStoreUser: () => dispatch( UserAction.cleanStoreUser() ),
    cleanStoreError: () => dispatch( UserAction.cleanStoreError() )
  }
}

const CreateUserComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateUser)


export default CreateUserComponent