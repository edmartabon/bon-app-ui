import React from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Row,
  Col,
  Button
} from "reactstrap";
import ListUser from './ListUser';
import CreateUser from './CreateUser';
import { UserAction } from '../../../../actions';

class Lists extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      createUserData: {},
      userModal: false
    };
    this.userModalToggle = this.userModalToggle.bind(this);
  }

  componentWillMount() {
    this.props.getUser()
  }


  userModalToggle() {
    this.setState(prevState => ({
      userModal: !prevState.userModal
    }));
  }

  render() {
    return (
      <div className="content">
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <CardTitle tag="h4">
                  List of Employees
                  <small className="float-sm-right card-title-action">
                    <Button color="primary" size="sm" onClick={this.userModalToggle}><i className="nc-icon nc-simple-add"></i></Button>
                  </small>
                </CardTitle>
              </CardHeader>
              <CardBody>
                <ListUser />
              </CardBody>
            </Card>
          </Col>
          <Col xs={12}>
          </Col>
        </Row>
        <div>
          <CreateUser
            isOpen={this.state.userModal}
            toggleFn={this.userModalToggle}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user, bot, department }) => {
  return {
    users: user.data,
    userError: user.error,
    bots: bot.bots,
    departments: department.departments,
    isUserFetching: user.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getUser: () => dispatch ( UserAction.get() ),
    postUser: (data) => dispatch( UserAction.post(data) ),
    cleanUserError: () => dispatch( UserAction.cleanError() ),
    cleanuserStore: () => dispatch( UserAction.cleanStoreUser() )
  }
}

const UserListComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Lists)

export default withRouter(UserListComponent)
