import React from "react";
import { connect } from 'react-redux';
import {
  Table,
  Button,
} from "reactstrap";
import UpdateUser from './UpdateUser';

class ListUser extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      userModal: false,
      selectedRow: null
    };
    
    this.userModalToggle = this.userModalToggle.bind(this);
  }

  userModalToggle() {
    this.setState(prevState => ({
      userModal: !prevState.userModal,
    }));
  }

  render() {
    const { users } = this.props;
    return(
      <React.Fragment>
        <Table responsive striped>
          <thead className="text-primary">
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Department</th>
              <th className="text-right">Action</th>
            </tr>
          </thead>
          <tbody>
            {users.map((prop, key) => {
              return (
                <tr key={ key }>
                  <td>{ prop.first_name } { prop.last_name }</td>
                  <td>{ prop.email }</td>
                  <td>{ prop.department ? prop.department.name : '' }</td>
                  <td>
                    <Button
                      size='sm'
                      color='primary'
                      onClick={ () => { 
                        this.setState({
                          selectedRow: prop._id
                        })
                        this.userModalToggle()
                      } }
                    >
                      <i className='nc-icon nc-ruler-pencil' />
                    </Button>
                  </td>
                </tr>
              );
            })}
            {users.length === 0 && (
              <tr className="no-data">
                <td colSpan="4" className="text-center"><i className="nc-icon nc-box"></i><div>No data found.</div></td>
              </tr>
            )}
          </tbody>
        </Table>
        <UpdateUser
          isOpen={this.state.userModal}
          selectedUserId={this.state.selectedRow}
          toggleFn={this.userModalToggle}
        />
      </React.Fragment>
    ) 
  }
}

const mapStateToProps = ({ user }) => {
  return {
    users: user.docs,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const ListUserComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(ListUser)


export default ListUserComponent