import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import routes from '../../../routes/login';
import '../../../assets/scss/custom.css';

class Login extends Component {

  render() {
    return (
      <div className='wrapper login-component'>
        <div className="login-component-panel">
          { this.props.children }
          { this.route() }
        </div>
      </div>
    );
  }

  route() {
    return (
      <Switch>
        {routes.map((prop, key) => {
          return <Route 
            path={ prop.path } 
            component={ prop.component } 
            key={ key } 
          />
        })}
      </Switch>
    );
  }

}

export default Login
