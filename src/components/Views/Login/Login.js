import React from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col,
  Alert
} from "reactstrap";
import Button from "components/CustomButton/CustomButton.jsx";
import FormInputs from "components/FormInputs/FormInputs.jsx";
import { AuthAction } from '../../../actions';

class Login extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      email: '',
      password: '',
      adminLink: '/admin/dashboard'
    }
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { token } = nextProps;
    if (token) {
      Cookie.set('token', token, { expires: 1 });
      nextProps.history.push(prevState.adminLink);
    }
    return null;
  }

  componentWillUnmount() {
    this.props.clean();
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { email, password } = this.state;
    this.props.login({ email, password });
  }


  render() {
    const { isFetching, error } = this.props;
    return (
      <Row>
        <Col sm="12" md={{ size: 6, offset: 3 }}>
          { error.message &&
            <Alert color="danger" className='text-center'>
              <span><b>Email or password incorrect</b></span>
            </Alert>
          }
          <Card>
            <CardHeader>
              <CardTitle className="text-center login-component-title">LOGIN</CardTitle>
            </CardHeader>
            <CardBody>
              <form onSubmit={this.handleSubmit}>
                <FormInputs
                  ncols={["col-md-12"]}
                  proprieties={[
                    {
                      inputProps: {
                        name: 'email',
                        type: "text",
                        placeholder: "Email...",
                        onChange: this.handleChange
                      },
                      addonLeft: (<i className="nc-icon nc-single-02 login-component-icon"></i>),
                      inputGroupAddonProps: {
                        addonType: 'prepend'
                      }
                    }
                  ]}
                />

                <FormInputs
                  ncols={["col-md-12"]}
                  proprieties={[
                    {
                      inputProps: {
                        name: 'password',
                        type: "password",
                        placeholder: "Password...",
                        onChange: this.handleChange
                      },
                      addonLeft: (<i className="nc-icon nc-key-25 login-component-icon"></i>),
                      inputGroupAddonProps: {
                        addonType: 'prepend'
                      }
                    }
                  ]}
                />

                <div className="update ml-auto mr-auto text-center">
                  <Button color="primary" round wd fill >
                    Login &nbsp;
                    { isFetching &&
                      <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt='loading' />
                    }
                  </Button>

                  
                </div>

                <div class="google-btn">
                    <div class="google-icon-wrapper">
                      <img class="google-icon-svg" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"/>
                    </div>
                    <p class="btn-text"><b>Sign in with Google</b></p>
                  </div>
              </form>
            </CardBody>
            
          </Card>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
    token: auth.token,
    error: auth.error,
    isFetching: auth.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: data => dispatch( AuthAction.login(data) ),
    clean: () => dispatch( AuthAction.clean() )
  }
}

const LoginComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

export default withRouter(LoginComponent)