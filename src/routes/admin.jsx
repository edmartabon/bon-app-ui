import User from "components/Views/Admin/Users/Lists";

var dashRoutes = [
  {
    path: "/admin/employees",
    name: "Employees",
    icon: "nc-icon nc-single-02",
    component: User 
  },
  {
    path: "/admin/employees1",
    name: "Error Reporting",
    icon: "nc-icon nc-circle-10",
    component: User 
  },
  { redirect: true, path: "/", pathTo: "/admin/employees", name: "Employees" }
];
export default dashRoutes;
