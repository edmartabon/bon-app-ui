import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import store from '../store';
import Cookie from 'js-cookie'
const { auth } = store.getState();

export const PrivateRoute = ({
  component: Component,
  ...rest
}) => {
    
  return (
    <Route
      {...rest} render={props => {
        if (auth.token || Cookie.get('token')) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
          );
        }
      }}
    />
  );
};
