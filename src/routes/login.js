import Login from "components/Views/Login/Login";

var dashRoutes = [
  {
    path: "/login",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Login
  }
];
export default dashRoutes;
