import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import {PrivateRoute} from './routes/protected';
import AdminLayout from './components/Views/Admin/Layout';
import LoginLayout from './components/Views/Login/Layout';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className='App'>
          <PrivateRoute path='/admin' component={ AdminLayout } /> 
          <Route path='/login' component={ LoginLayout } /> 
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
