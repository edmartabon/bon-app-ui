import http from '../services/http'
import { 
  GET_BOT_SUCCESS,
  GET_BOT_FAILURE,
  BOT_FETCHING,
  BOT_RESET,
} from '../variables/BotActionType'


const actions = {

  get(data) {
    return dispatch => {
      dispatch({ type: BOT_FETCHING, payload: true });
      http.get('/api/bots', data)
        .then(success) 
        .catch(error);

      function success(res) {
        const data = res.data;
        dispatch({ type: GET_BOT_SUCCESS, payload: data });
      }
      
      function error(err) {
        dispatch({ type: GET_BOT_FAILURE, payload: err.response.data });
      };
    }
  },

  post(data) {
    return dispatch => {
      
    }
  },

  clean() {
    return dispatch => {
      dispatch({ type: BOT_RESET })
    }
  }

}

export default actions;