import Cookie from 'js-cookie'
import http from '../services/http'
import { 
  GET_AUTH_TOKEN,
  GET_AUTH_SUCCESS,
  GET_AUTH_FAILURE,
  CLEAN_AUTH_REQUEST,
  LOGOUT_AUTH_REQUEST
} from '../variables/AuthActionType'


const actions = {

  login(data) {
    return dispatch => {
      dispatch({ type: GET_AUTH_TOKEN });
      http.post('/api/auth/login', data)
        .then(success) 
        .catch(error);

      function success(res) {
        const { message } = res.data;
        Cookie.set('token', message, { expires: 1 });
        dispatch({ type: GET_AUTH_SUCCESS, payload: message });
      }
      function error(err) {
        dispatch({ type: GET_AUTH_FAILURE, payload: err.response.data });
      };
    }
  },

  logout(data) {
    return dispatch => {
      Cookie.remove('token');
      http.post('/api/auth/logout', data)
        .finally(response);

      function response() {
        dispatch({ type: LOGOUT_AUTH_REQUEST });
        dispatch({ type: CLEAN_AUTH_REQUEST });
      }
    }
  },

  clean() {
    return dispatch => {
      dispatch({ type: CLEAN_AUTH_REQUEST })
    }
  }

}

export default actions;