import http from '../services/http'
import { 
  GET_DEPARTMENT_SUCCESS,
  GET_DEPARTMENT_FAILURE,
  DEPARTMENT_FETCHING,
  DEPARTMENT_RESET,
} from '../variables/DepartmentActionType'


const actions = {

  get(data) {
    return dispatch => {
      dispatch({ type: DEPARTMENT_FETCHING, payload: true });
      http.get('/api/departments', data)
        .then(success) 
        .catch(error);

      function success(res) {
        const data = res.data
        dispatch({ type: GET_DEPARTMENT_SUCCESS, payload: data });
      }
      
      function error(err) {
        dispatch({ type: GET_DEPARTMENT_FAILURE, payload: err.response.data });
      };
    }
  },

  post(data) {
    return dispatch => {
      
    }
  },

  clean() {
    return dispatch => {
      dispatch({ type: DEPARTMENT_RESET })
    }
  }

}

export default actions;