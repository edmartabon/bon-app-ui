import http from '../services/http'
import { 
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  USER_RESET,
  USER_FETCHING,
  USER_RESET_ERROR,

  USER_STORE_SUCESS,
  USER_STORE_FAILURE,
  USER_STORE_FETCHING,
  USER_STORE_RESET,
  USER_RESET_STORE_ERROR,

  USER_UPDATE_SUCESS,
  USER_UPDATE_FAILURE,
  USER_UPDATE_FETCHING,
  USER_UPDATE_RESET,
  USER_RESET_UPDATE_ERROR,
} from '../variables/UserActionType'

const actions = {

  // Get
  get(data) {
    return dispatch => {
      dispatch({ type: USER_FETCHING, payload: true });
      http.get('/api/users')
        .then(success) 
        .catch(error);

      function success(res) {
        const data = res.data;
        dispatch({ type: GET_USER_SUCCESS, payload: data });
      }
      
      function error(err) {
        dispatch({ type: GET_USER_FAILURE, payload: err.response.data });
      };
    }
  },

  clean() {
    return dispatch => {
      dispatch({ type: USER_RESET })
    }
  },

  cleanError() {
    return dispatch => {
      dispatch({ type: USER_RESET_ERROR })
    }
  },

  // Store
  post(data) {
    return dispatch => {
      dispatch({ type: USER_STORE_FETCHING, payload: true });
      http.post('/api/users', data)
        .then(success) 
        .catch(error);

      function success(res) {
        const data = res.data;
        dispatch({ type: USER_STORE_SUCESS, payload: data });
      }
      
      function error(err) {
        dispatch({ type: USER_STORE_FAILURE, payload: err.response.data.message });
      };
    }
  },

  cleanStoreError() {
    return dispatch => {
      dispatch({ type: USER_RESET_STORE_ERROR })
    }
  },

  cleanStoreUser() {
    return dispatch => {
      dispatch({ type: USER_STORE_RESET })
    }
  },

  // Update 
  update(data) {
    return dispatch => {
      dispatch({ type: USER_UPDATE_FETCHING, payload: true });
      http.put(`/api/users/${data._id}`, data)
        .then(success) 
        .catch(error);

      function success(res) {
        const updatedUser = res.data;
        dispatch({ type: USER_UPDATE_SUCESS, payload: updatedUser });
      }
      
      function error(err) {
        dispatch({ type: USER_UPDATE_FAILURE, payload: err.response.data.message });
      };
    }
  },

  cleanUpdateError() {
    return dispatch => {
      dispatch({ type: USER_RESET_UPDATE_ERROR })
    }
  },

  cleanUpdateUser() {
    return dispatch => {
      dispatch({ type: USER_UPDATE_RESET })
    }
  },

}

export default actions;