export const GET_BOT_SUCCESS = 'GET_BOT_SUCCESS';
export const GET_BOT_FAILURE = 'GET_BOT_FAILURE';
export const BOT_FETCHING    = 'BOT_FETCHING';
export const BOT_RESET       = 'BOT_RESET';