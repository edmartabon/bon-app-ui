const Environment = {

  env: 'staging',

  staging: {
    BASE_URL: 'http://localhost:4001'
  },

  production: {
    BASE_URL: 'http://edmart.bon.local'    
  }

}

export default Environment;