import axios from 'axios';
import store from '../store';
import environment from '../variables/Environment';

const env = environment.env;
const { auth } = store.getState();

const config = {
  baseURL: environment[env].BASE_URL,
  header: {
    'Content-Type': 'application/json'
  }
}

if (auth.token) {
  config.header['Authorization'] = auth.token;
}

const http = axios.create(config);

http.interceptors.response.use(res => {
  return res;
}, function (err) {
  const newErr = { ...err };
  
  if (!newErr.response) {
    newErr.response = {
      status: 500,
      data: {
        message: { error: ['Invalid Request'] }
      }
    }
  } 
  return Promise.reject(newErr);
});

export default http;