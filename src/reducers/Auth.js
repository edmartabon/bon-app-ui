import Cookie from 'js-cookie'
import {
  GET_AUTH_TOKEN,
  GET_AUTH_SUCCESS,
  GET_AUTH_FAILURE,
  CLEAN_AUTH_REQUEST,
  LOGOUT_AUTH_REQUEST
} from '../variables/AuthActionType'

const token = Cookie.get('token')

const initialState = {
  token,
  user: {},
  error: {},
  isFetching: false,
}

export function auth(state = initialState, action) {
  switch(action.type) {
    case GET_AUTH_TOKEN:
      return { ...state, isFetching: true, error: {} }
    case GET_AUTH_SUCCESS:
      return { ...state, token: action.payload, isFetching: false };
    case GET_AUTH_FAILURE:
      return { ...state, token: null, user: {}, isFetching: false, error: action.payload };
    case LOGOUT_AUTH_REQUEST:
      return { ...state, token: null, user: {}, isFetching: false, error: {} }
    case CLEAN_AUTH_REQUEST:
      return { ...state, isFetching: false, error: {} }
    default:
      return { ...state };
  }
}
