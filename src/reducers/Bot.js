import {
  GET_BOT_SUCCESS,
  GET_BOT_FAILURE,
  BOT_FETCHING,
  BOT_RESET,
} from '../variables/BotActionType'

const initialState = {
  bots: [],
  error: {},
  isFetching: false,
}

export function bot(state = initialState, action) {
  switch(action.type) {
    case GET_BOT_SUCCESS:
      return { ...state, bots: action.payload, isFetching: false };
    case GET_BOT_FAILURE: 
      return { ...state, error: action.payload, isFetching: false };
    case BOT_FETCHING:
      return { ...state, isFetching: action.payload }
    case BOT_RESET:
      return { ...state, bots: [], error: {}, isFetching: false }
    default:
      return { ...state };
  }
}
