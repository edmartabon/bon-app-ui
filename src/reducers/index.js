import { combineReducers } from 'redux'
import { auth } from './Auth';
import { user } from './User';
import { bot } from './Bot';
import { department } from './Department';

export default combineReducers({
  auth,
  user,
  bot,
  department
});