import {
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  USER_RESET,
  USER_FETCHING,
  USER_RESET_ERROR,

  USER_STORE_SUCESS,
  USER_STORE_FAILURE,
  USER_STORE_FETCHING,
  USER_STORE_RESET,
  USER_RESET_STORE_ERROR,

  USER_UPDATE_SUCESS,
  USER_UPDATE_FAILURE,
  USER_UPDATE_FETCHING,
  USER_UPDATE_RESET,
  USER_RESET_UPDATE_ERROR
} from '../variables/UserActionType'

const initialState = {
  // Get
  docs: [],
  limit: 0,
  page: 1,
  pages: 1,
  total: 0,
  error: {},
  isFetching: false,

  storeUser: {},
  storeError: {},
  isStoreFetching: false,

  updateUser: {},
  updateError: {},
  isUpdateFetching: false
}

export function user(state = initialState, action) {
  switch(action.type) {
    // Get Process
    case GET_USER_SUCCESS:
      return { ...state, ...action.payload, isFetching: false }
    case GET_USER_FAILURE: 
      return { ...state, error: action.payload, isFetching: false };
    case USER_FETCHING:
      return { ...state, isFetching: action.payload }
    case USER_RESET:
      return { ...state, docs: [], limit: 0, page: 1, pages: 1, total: 0, error: {}, isFetching: false }
    
    
    // Store Process
    case USER_STORE_SUCESS:
      return { ...state, storeUser: action.payload, storeError: {}, isStoreFetching: false };
    case USER_STORE_FAILURE: 
      return { ...state, storeUser: {}, storeError: action.payload, isStoreFetching: false };
    case USER_STORE_FETCHING:
      return { ...state, isStoreFetching: action.payload };
    case USER_STORE_RESET: 
      return { ...state, storeUser: {}, isStoreFetching: false, storeError: {} };
    case USER_RESET_STORE_ERROR:
      return { ...state, storeError: {}, isStoreFetching: false };

    // Update Process
    case USER_UPDATE_SUCESS:
      const updateUser = action.payload;
      const docs = state.docs.map(x => {
        return x._id === updateUser._id ? updateUser : x
      });
      return { ...state, docs, updateUser, updateError: {}, isUpdateFetching: false }
    case USER_UPDATE_FAILURE: 
      return { ...state, updateUser: {}, updateError: action.payload, isUpdateFetching: false };
    case USER_UPDATE_FETCHING:
      return { ...state, isUpdateFetching: action.payload };
    case USER_UPDATE_RESET: 
      return { ...state, updateUser: {}, isUpdateFetching: false, updateError: {} }
    case USER_RESET_UPDATE_ERROR:
      return { ...state, updateError: {}, isUpdateFetching: false };

    // Clean Data
    case USER_RESET_ERROR:
      return { ...state, error: {} }
    default:
      return { ...state };
  }
}
