import {
    GET_DEPARTMENT_SUCCESS,
    GET_DEPARTMENT_FAILURE,
    DEPARTMENT_FETCHING,
    DEPARTMENT_RESET,
  } from '../variables/DepartmentActionType'
  
  const initialState = {
    departments: [],
    error: {},
    isFetching: false,
  }
  
  export function department(state = initialState, action) {
    switch(action.type) {
      case GET_DEPARTMENT_SUCCESS:
        return { ...state, departments: action.payload, isFetching: false };
      case GET_DEPARTMENT_FAILURE: 
        return { ...state, error: action.payload, isFetching: false };
        case DEPARTMENT_FETCHING:
        return { ...state, isFetching: action.payload }
      case DEPARTMENT_RESET:
        return { ...state, departments: [], error: {}, isFetching: false }
      default:
        return { ...state };
    }
  }
  